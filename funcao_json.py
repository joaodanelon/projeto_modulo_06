import pandas as pd

def converte_json(df: pd.DataFrame, filename: str):
    # Seleciona as colunas numéricas
    colunas_numericas = df.select_dtypes(['float64','int64'])

    # Converte as colunas numéricas para string
    for coluna in colunas_numericas:
        df[coluna] = df[coluna].astype(str)
    df.to_json(f'{filename}.json', orient='records', indent=2)

    print("Arquivo JSON criado com sucesso!")
