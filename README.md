# Gerador de notas fiscais

## Objetivo

Este projeto tem como objetivo desenvolver um programa com função de emitir notas fiscais em diferentes formatos.
O programa lê um arquivo csv com os dados dos produtos e gera arquivos XML, JSON ou YAML com as informações da nota fiscal.

## Requisitos

-   Python 3.9 ou superior
-   Instale os requisitos com o comando: `pip install -r requirements.txt`

## Uso

-   Para executar o programa, use o comando: `python gerador-nf.py --formato=[xml, json, yaml] [file.csv]`
-   O argumento `--formato` especifica o formato de saída desejado. Pode ser um entre `xml`, `json` ou `yaml`.
-   O argumento `file.csv` é opcional e especifica o arquivo csv de entrada com os dados dos produtos. Se não for informado, o programa usará o arquivo `entrada.csv` por padrão.
-   Os arquivos de saída serão gerados na mesma pasta do programa com o mesmo nome do arquivo csv. Por padrão, conversões para XML, JSON e YAML resultarão em arquivos `entrada.xml`, `entrada.json` e `entrada.yaml`, respectivamente.

## Estrutura do projeto

-   O script principal é o `gerador-nf.py`, que contém a lógica principal do programa e recebe os argumentos por linha de comando.
-   Os arquivos `funcao_xml.py`, `funcao_json.py` e `funcao_yaml.py` contêm as funções responsáveis pela conversão dos dados para os respectivos formatos.
-   O arquivo `entrada.csv` contém os dados de exemplo dos produtos em formato tabular.
-   O arquivo `.gitignore` contém as regras para ignorar arquivos desnecessários no controle de versão.

## Contribuições

-   Se você quiser contribuir para este projeto, faça uma branch nova e uma pull request para a branch `dev`.
-   Os commits devem seguir o padrão de abreviações: `[ADD]` para adições, `[CHG]` para alterações e `[DEL]` para deleções.
-   Descreva as suas alterações de forma clara e objetiva no corpo da pull request.

## Autores

-   Gisele Beltramini
-   João Gabriel Danelon
-   João Victor Cadiolli
-   Naiara da Gama
-   Paula Gouveia
