import pandas as pd
import xml.etree.ElementTree as ET

def converte_xml(df: pd.DataFrame, filename: str):
    root = ET.Element("listaDeProdutos")

    for _, linha in df.iterrows():
        produto = ET.SubElement(root, 'produto')
        for coluna in df.columns:
            sub_elemento = ET.SubElement(produto, coluna)
            sub_elemento.text = str(linha[coluna])

    # Cria a árvore XML
    tree = ET.ElementTree(root)
    ET.indent(tree, space='\t', level=0)

    # Escreve o arquivo XML
    tree.write(f'{filename}.xml')

    print("Arquivo XML criado com sucesso!")
