import pandas as pd
import yaml

def converte_yaml(df: pd.DataFrame, filename: str):
    dicionarios = df.to_dict(orient='records')

    for dicionario in dicionarios:
        for coluna in dicionario:
            if (isinstance(dicionario[coluna], int) or isinstance(dicionario[coluna], float)):
                dicionario[coluna] = str(dicionario[coluna])

    text = yaml.dump(dicionarios, sort_keys=True, default_flow_style=False)

    with open(f'{filename}.yaml', 'w') as f:
        f.write(text)

    print("Arquivo YAML criado com sucesso!")
