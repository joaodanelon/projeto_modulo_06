#!.\gerador-nf\Scripts\python.exe

import sys
import pandas as pd

from funcao_xml import converte_xml
from funcao_json import converte_json
from funcao_yaml import converte_yaml

argc = len(sys.argv)

if (argc < 2 or argc > 3):
    print('Erro: número de argumentos incorreto!')
    print('Uso: python gerador-nf.py --formato=[xml, json, yaml] [file].csv')
    exit(1)

if (not sys.argv[1].startswith('--formato=')):
    print('Erro: argumento "' + sys.argv[1] + '" é inválido!')
    print('Uso: python gerador-nf.py --formato=[xml, json, yaml] [file].csv')
    exit(1)

arg = sys.argv[1][10:]
if (arg != 'xml' and arg != 'json' and arg != 'yaml'):
    print('Erro: extensão "' + arg + '" inválida!')
    print('Uso: python gerador-nf.py --formato=[xml, json, yaml] [file].csv')
    exit(1)

if (argc == 3):
    file = sys.argv[2]
else:
    file = 'entrada.csv'

if (file[-4:] != '.csv'):
    print('Erro: extensão "' + file[-4:] + '" do arquivo "' + file + '" inválida!')
    print('Uso: python gerador-nf.py --formato=[xml, json, yaml] [file].csv')
    exit(1)

try:
    df = pd.read_csv(file)
except FileNotFoundError:
    print('Erro: arquivo "' + file + '" não encontrado!')
    exit(1)

# redirecionar variavel para funcao responsavel
match (arg):
    case 'xml':
        converte_xml(df, file[:-4])
    case 'json':
        converte_json(df, file[:-4])
    case 'yaml':
        converte_yaml(df, file[:-4])
